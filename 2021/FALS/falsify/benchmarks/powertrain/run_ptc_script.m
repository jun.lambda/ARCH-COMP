% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)

% Power Train Control Benchmark
%%%%%%%%%%%%%%%%%%%%

tmpl = struct();
tmpl.mdl = 'AbstractFuelControl_M1';
tmpl.output_range = [-0.01 0.01; 0.0 1.0; 12.5 16.5; 12.5 16.5; 0.0 81.2; 900.0 1100.0];
tmpl.init_opts = {{'simTime', 50},...
    {'measureTime', 1}, {'fault_time', 60}, {'spec_num', 1},...
    {'fuel_inj_tol', 1.0}, {'MAF_sensor_tol', 1.0}, {'AF_sensor_tol', 1.0}};
tmpl.gen_opts = {{'en_speed', [900 1099.999]}};
tmpl.interpolation = {'pconst'};
tmpl.option = {};
tmpl.maxEpisodes = maxEpisodes;
tmpl.agentName = 'Falsifier';
tmpl.sampleTime = 5;

%Formula 27
fml27 = struct(tmpl);
fml27.input_range = [0.0 61.1];
fml27.expName = '(AFC27 0.008)';
fml27.system = 'AFC_normal'
fml27.targetFormula = '[]_[11,50]((((!nlow) /\ <>_[0,0.05] !nhigh) \/ ((!nhigh) /\ <>_[0,0.05] !nlow)) -> []_[1,5] ((!nmuL) /\ !nmuH))';
fml27.monitoringFormula = '[.]_[500,500](((!nlow) /\ <>_[0,5] !nhigh) \/ ((!nhigh) /\ <>_[0,5] !nlow)) -> []_[100,500] ((!nmuL) /\ !nmuH)';
fml27.preds(1).str = 'nlow'; % !(throttle < 8.8) i.e. throttle >= 8.8
fml27.preds(1).A = [0 0 0 0 -1 0];
fml27.preds(1).b = -8.8;
fml27.preds(2).str = 'nhigh'; % !(throttle > 40) i.e. throttle <= 40
fml27.preds(2).A = [0 0 0 0 1 0];
fml27.preds(2).b = 40.0;
fml27.preds(3).str = 'nmuL'; % !(mu > 0.008) i.e. mu <= 0.008
fml27.preds(3).A = [1 0 0 0 0 0];
fml27.preds(3).b = 0.008;
fml27.preds(4).str = 'nmuH'; % !(mu < -0.008) i.e. mu >= -0.008
fml27.preds(4).A = [-1 0 0 0 0 0];
fml27.preds(4).b = 0.008;
fml27.stopTime = 50;

%Formula 29
fml29 = struct(tmpl);
fml29.expName = '(AFC29 0.007)';
fml29.input_range = [0.0 61.1];
fml29.system = 'AFC_normal'
fml29.targetFormula = '[]_[11,50]((!nmuL) /\ !nmuH)';
fml29.monitoringFormula = '(!nmuL) /\ !nmuH';
fml29.preds(1).str = 'nmuL'; % !(mu > 0.007) i.e. mu <= 0.007
fml29.preds(1).A = [1 0 0 0 0 0];
fml29.preds(1).b = 0.007;
fml29.preds(2).str = 'nmuH'; % !(mu < -0.007) i.e. mu >= -0.007
fml29.preds(2).A = [-1 0 0 0 0 0];
fml29.preds(2).b = 0.007;
fml29.stopTime = 50;

%Formula 33
fml33 = struct(tmpl);
fml33.expName = '(AFC33 0.007)';
fml33.input_range = [61.2 81.2];
fml33.system = 'AFC_power'
fml33.targetFormula = '[]_[11,50]((!nmuL) /\ !nmuH)';
fml33.monitoringFormula = '(!nmuL) /\ !nmuH';
fml33.preds(1).str = 'nmuL'; % !(mu > 0.007) i.e. mu <= 0.007
fml33.preds(1).A = [1 0 0 0 0 0];
fml33.preds(1).b = 0.007;
fml33.preds(2).str = 'nmuH'; % !(mu < -0.007) i.e. mu >= -0.007
fml33.preds(2).A = [-1 0 0 0 0 0];
fml33.preds(2).b = 0.007;
fml33.stopTime = 50;

formulas = {fml27, fml29, fml33};

configs = { };
for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.algoName = algorithms{i};
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end

do_experiment('PTC', configs);
