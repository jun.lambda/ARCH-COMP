function [polytopes]=segmentPolytope(Obj,varargin)
% Purpose:  return polytope of actual segment
% Pre:      partition object
% Post:     polytope object
% Tested:   14.09.06,MA
% Modified: 31.7.17, AP

%generate polytope out of cell--------------------------
if nargin > 1
    [intervals]=segmentInterval(Obj,varargin{1});
else
    [intervals]=segmentInterval(Obj);
end


for i = 1:length(intervals)
    IH=intervals{i};
    polytopes{i} = polytope(IH);
end
%-------------------------------------------------------- 