T1 = [[]; generate_table('NN', 3)];
T2 = [[]; generate_table('NN', 14)];
T2 = [T2; generate_table('PTC', 5)];
T1 = [T1; generate_table('SteamCondenser', 0.1)];
T2 = [T2; generate_table('SteamCondenser', 1.75)];
T1 = [T1; generate_table('wind_turbine', 1)];
T2 = [T2; generate_table('wind_turbine', 5)];
T1 = [T1; generate_table('transmission', 1)];
T2 = [T2; generate_table('transmission', 5)];
T1 = [T1; generate_table('cars', 1)];
T2 = [T2; generate_table('cars', 5)];

T1 = sortrows(T1, {'system', 'algorithm', 'property'});
T1.instance = repmat (1, size (T1, 1), 1);
% writetable(T1, 'data/result/Instance1.csv', 'QuoteStrings', true);

T2 = sortrows(T2, {'system', 'algorithm', 'property'});
T2.instance = repmat (2, size (T2, 1), 1);
% writetable(T2, 'data/result/Instance2.csv', 'QuoteStrings', true);
writetable ([T1; T2], 'data/result/FALS-2022-falsify.csv', 'QuoteStrings', true);

function [T] = generate_table(model, sampleTime)
    global logDir
    table_name = [logDir, '/', model, '.csv'];
    if isfile(table_name)
        T = readtable(table_name);
        rows = T.sampleTime == sampleTime;
        T = T(rows, :);
        T.Properties.VariableNames = {'id', 'system', 'property', 'algorithm', 'sampleTime', 'simulations', 'total time', 'robustness'};
        T.falsified = T.robustness < 0;
        if (size(T,1) == 0)
            % Make dummy columns to avoid dimension mismatches
            T.stop_time = T.time;
            T.input = T.time;
        end
        for i = 1:size(T, 1)
            prop = T.property(i);
            algorithm = T.algorithm(i);
            id = T.id(i);
            filename = [logDir, '/', model, '-', prop{1}, '-', algorithm{1}, '-', num2str(id), '.mat'];
            load(filename, 'bestXout', 'bestOpts', 'config');
            T.stop_time(i) = config.stopTime;
            T.system(i) = {config.system};
            %{
            if (isequal(model, 'SteamCondenser'))
                bestXout = timeseries(squeeze(bestXout.Data), bestXout.Time);
            end
            %}
            control_points = 0:T.sampleTime(i):bestXout.Time(end);
            %X = resample(bestXout, control_points);
            X = bestXout;
            inputs = reshape(X.Data, [size(X.Data,1),size(X.Data,3)]);
            if (isequal(model, 'PTC'))
                % attach constant input
                inputs = [inputs; repmat(bestOpts{2}, 1, size(inputs,2))];
            end
            T.input(i) = {mat2str([X.Time, inputs'])};
        end
    else
        T = [];
    end
    for i = 1:size(T,2)
        if (strcmp (T.Properties.VariableNames(i), 'stop_time'))
            T.Properties.VariableNames(i) = {'stop time'};
        end
    end
end
    
