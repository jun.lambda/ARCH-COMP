% (C) 2019 National Institute of Advanced Industrial Science and Technology
% (AIST)
tmpl = struct();
tmpl.mdl = 'cars';
tmpl.system = 'CC';
tmpl.init_opts = {};
tmpl.gen_opts = {};
tmpl.option = {};
tmpl.maxEpisodes = maxEpisodes;
tmpl.agentName = 'Falsifier';
tmpl.interpolation = {'pconst'};
tmpl.stopTime = 100;
tmpl.input_range = [0.0 1.0; 0.0 1.0];
tmpl.output_range = [-100 0; -100 10; -100 20; -100 30; -100 40];

% Formula 0
% Spacing
fml0 = struct(tmpl);
fml0.expName = 'CC0';
fml0.targetFormula = '[]_[0,50]((!p12) /\ (!p23) /\ (!p34) /\ (!p45))';
fml0.monitoringFormula = '(!p12) /\ (!p23) /\ (!p34) /\ (!p45)';

fml0.preds(1).str = 'p12'; % y2-y1 <= 7.5
fml0.preds(1).A = [-1 1 0 0 0];
fml0.preds(1).b = 7.5;
fml0.preds(2).str = 'p23';
fml0.preds(2).A = [0 -1 1 0 0];
fml0.preds(2).b = 7.5;
fml0.preds(2).str = 'p34';
fml0.preds(2).A = [0 0 -1 1 0];
fml0.preds(2).b = 7.5;
fml0.preds(2).str = 'p45';
fml0.preds(2).A = [0 0 0 -1 1];
fml0.preds(2).b = 7.5;
fml0.stopTime = 50;

% Formula 1
% Invariant
fml1 = struct(tmpl);
fml1.expName = 'CC1';
fml1.targetFormula = '[]p1';
fml1.monitoringFormula = 'p1';

fml1.preds(1).str = 'p1';
fml1.preds(1).A = [0 0 0 -1 1];
fml1.preds(1).b = 40.0;

% Formula 2
% Guarantee
fml2 = struct(tmpl);
fml2.expName = 'CC2';
fml2.targetFormula = '[]_[0,70]<>_[0,30]p1';
fml2.monitoringFormula = '[.]_[300,300]<>_[0,300]p1';

fml2.preds(1).str = 'p1';
fml2.preds(1).A = [0 0 0 1 -1];
fml2.preds(1).b = -15;

%Formula 3
% Obligation
fml3 = struct(tmpl);
fml3.expName = 'CC3';
fml3.targetFormula = '[]_[0,80](([]_[0,20]p1) \/ (<>_[0,20]p2))';
fml3.monitoringFormula = '[.]_[200.0,200.0]((([]_[0,200]p1) \/ (<>_[0,200]p2)))';

fml3.preds(1).str = 'p1';
fml3.preds(1).A = [-1 1 0 0 0];
fml3.preds(1).b = 20;
fml3.preds(2).str = 'p2'; % y5-y4 >= 40
fml3.preds(2).A = [0 0 0 1 -1];
fml3.preds(2).b = -40;

%Formula 4
%Persistence
fml4 = struct(tmpl);
fml4.expName = 'CC4';
fml4.targetFormula = '[]_[0,65]<>_[0,30][]_[0,5]p1';
fml4.monitoringFormula = '[.]_[350,350]<>_[0,300][]_[0,50]p1';
fml4.preds(1).str = 'p1';
fml4.preds(1).A = [0 0 0 1 -1];
fml4.preds(1).b = -8;

%Formula 5
%Reactivity
fml5 = struct(tmpl);
fml5.expName = 'CC5';
fml5.targetFormula = '[]_[0,72]<>_[0,8]([]_[0,5]p1 -> []_[5,20]p2)';
fml5.monitoringFormula = '[.]_[280,280]<>_[0,80]([]_[0,50]p1 -> []_[50,200]p2)';
fml5.preds(1).str = 'p1';
fml5.preds(1).A = [1 -1 0 0 0];
fml5.preds(1).b = -9;
fml5.preds(2).str = 'p2';
fml5.preds(2).A = [0 0 0 1 -1];
fml5.preds(2).b = -9;

formulas = {fml1, fml2, fml3, fml4, fml5};

configs = { };
for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.algoName = algorithms{i};
        config.sampleTime = 1;
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end

for k = 1:size(formulas, 2)
    for i = 1:size(algorithms, 2)
        config = struct(formulas{k});
        config.algoName = algorithms{i};
        config.sampleTime = 5;
        for l = 1:maxIter
            configs = [configs, config];
        end
    end
end


do_experiment('cars', configs);
